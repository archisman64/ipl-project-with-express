const express = require('express');
const expressRequestId = require('express-request-id');
const app = express();

// Import and use the necessary middleware
const requestLogger = require('./server/middleware/logger');

const iplRoutes = require('./server/routes/iplRoutes');
const homeRoutes = require('./server/routes/homeRoutes');
const logRoutes = require('./server/routes/logRoutes');
const erorHandler = require('./server/middleware/errorHandler');

// app.use(express.static('public/views'));
app.use(expressRequestId());
app.use(requestLogger);
app.use('/', homeRoutes);
app.use('/api', iplRoutes);
app.use(logRoutes);
app.use((req, res, next) => {
    const htmlContent = `
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
            <h2>Requested endpoint is not supported!</h2>
            <h3>Please enter a valid endpoint or <a href="/">click here to go to home</a></h3>
        </body>
        </html>
    `;
    next({ statusCode: 404, message: htmlContent });
}, erorHandler);

// Export the Express app
module.exports = app;
