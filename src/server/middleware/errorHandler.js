const erorHandler = (err, req, res, next) => {
    console.log('my error handler ran');
    const statusCode = err.statusCode || 500;
    res.status(statusCode).send(err.message || { error: { message: 'Internal Server Error' } });
};

module.exports = erorHandler;