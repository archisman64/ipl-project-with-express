const path = require('path');
const fs = require('fs');
const pathToLogFile = path.join(__dirname, '../../../logs/requests.log');

const logger = (req, res, next) => {
    // console.log('logger ran', pathToLogFile);
    const logEntry = `${req.id} - ${new Date().toISOString()} - ${req.method} ${req.originalUrl}\n`;
    fs.appendFile(pathToLogFile, `${logEntry}\n`, (err) => {
        if(err) {
        console.log(err);
        next(err);
        } else {
        next();
        }
    })
}

module.exports = logger;