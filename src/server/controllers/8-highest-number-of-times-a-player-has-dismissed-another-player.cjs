//  Find the highest number of times one player has been dismissed by another player
const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

// const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
const delivieriesFilePath=path.join(__dirname, '../../data/deliveries.csv'); 
// const outputFilePath=path.join(__dirname, '../public/output/8-highest-number-of-times-a-player-has-dismissed-another-player.json');
exports.getPlayerDismissal = (req, res, next) => {
    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries) => {
        // creating valid/required dismissal map to filter out the dismissal types later
        const validDismissalTypes = {'caught': true, 'bowled': true, 'lbw': true, 'caught and bowled': true, 'stumped': true};
        
        const map= deliveries.reduce((acc, delivery) => {
            // extracting the required feilds for improved readablity
            const dismissal = delivery.dismissal_kind;
            const playerDismissed = delivery.player_dismissed.trim();
            const bowler = delivery.bowler.trim();
    
            if(bowler && playerDismissed && (dismissal in validDismissalTypes)){
                // creating an unique identifier for bowler-batsman combination
                // only when the batsman got dismissed by the bowler
                const unique_key = bowler + '+' + playerDismissed;
                if(!acc[unique_key]) {
                    // creating new entry when it doesn't exist
                    acc[unique_key] = 1;
                } else {
                    // incrementing existing entry
                    acc[unique_key] ++;
                }
            }
            return acc;
        }, {});
        // console.log('map:', map);
         
        // the key with the required information
        const requiredInfo = Object.entries(map).sort((key1, key2) => {
            return key2[1] - key1[1]
        })[0];
        // console.log(requiredInfo);
        // creating result object from the above info
        const finalObject = {
            'bowler': requiredInfo[0].split('+')[0],
            'batsman': requiredInfo[0].split('+')[1],
            'no_of_dismissals': requiredInfo[1]
        }
        // console.log(finalObject);
        res.json(finalObject);
        // fs.writeFileSync(outputFilePath, JSON.stringify(finalObject, null, 2), 'utf-8')
    })
    .catch((err) => {
        next(err)
    });

};