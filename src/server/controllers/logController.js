const path = require('path');
const pathToLogFile = path.join(__dirname, '../../../logs/requests.log');

exports.getRequestLogs = (req, res, next) => {
    // sending the contents of log file as response
    res.sendFile(pathToLogFile, (err) => {
        if(err) {
            next(err);
        }
    });
};