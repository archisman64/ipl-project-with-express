const path = require('path');

const homePath = path.join(__dirname, '../../public/views/home.html');

exports.getHome = (req, res, next) => {
    try {   
        res.sendFile(homePath);
    } catch (error) {
        next(error);
    }
};