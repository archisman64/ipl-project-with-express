//  Number of matches played per year for all the years in IPL
const csvToJson = require('csvtojson');
const path = require('path');
const fs = require('fs');

const matchesFile= path.join(__dirname, '../../data/matches.csv'); 
// console.log('------', matchesFile);
// const outputFilePath=path.join(__dirname, '../public/output/1-matches-per-year.json');

exports.getMatchesPerYear = (req, res, next) => {
    
    csvToJson()
    .fromFile(matchesFile)
    .then((result)=>{
        const matchesPerYear = result.reduce((acc, value) => { 
            const season = value.season;
            if(acc[season]) {
                acc[season] = acc[season] + 1;
            } else {
                acc[season] = 1;
            }
            return acc;
        }, {});
        res.json(matchesPerYear);
        // fs.writeFileSync(outputFilePath, JSON.stringify(matchesPerYear, null, 2), 'utf-8')
    })
    .catch(err => {
        next(err)
    });
};