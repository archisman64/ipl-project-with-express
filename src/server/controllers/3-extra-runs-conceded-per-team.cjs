//  Extra runs conceded per team in the year 2016

const csvToJson = require('csvtojson');
const path = require('path');
const fs = require('fs');

const matchesFilePath=path.join(__dirname, '../../data/matches.csv'); 
const delivieriesFilePath=path.join(__dirname, '../../data/deliveries.csv'); 
// const outputFilePath = path.join(__dirname, '../public/output/3-extra-runs-conceded-per-team.json');

exports.getExtraRunsPerTeam = (req, res, next) => {
    let matchIds;
    
    csvToJson()
    .fromFile(matchesFilePath)
    .then((result)=>{
        
        const filteredIds = result.reduce((acc, value) => {
            if(value.season && value.season === '2016') {
                acc.push(value.id);
            } else {
                acc = [...acc];
            }
            return acc;
        }, []);
        
        matchIds = [...filteredIds];
        // for(let index = 0; index < result.length; index ++) {
        //     if(result[index].season && result[index].season === '2016') {
        //         matchIds.push(result[index].id);
        //     }
        // }
        
        // console.log('match ids',  matchIds);
    })
    .catch(err => console.log(err));
    
    
    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries)=>{
    
        const extraRunsPerTeam = deliveries.reduce((acc, value) => {
            // console.log('value:', value);
            const bowlingTeam = value.bowling_team.trim();
            const extraRuns = +value.extra_runs;
            if(matchIds.includes(value.match_id)) {
                if(!acc[bowlingTeam]) {
                    acc[bowlingTeam] = extraRuns;
                } else {
                    acc[bowlingTeam] += extraRuns;
                }
            } else {
                acc = {...acc};
            }
            return acc;
        }, {});
        res.json(extraRunsPerTeam);
    //    fs.writeFileSync(outputFilePath, JSON.stringify(extraRunsPerTeam, null, 2), 'utf-8');
    })
    .catch(err => {
        next(err);
    });

};

