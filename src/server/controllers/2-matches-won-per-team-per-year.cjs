//  Number of matches won per team per year in IPL
const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');
const matchesFile= path.join(__dirname, '../../data/matches.csv'); 

// const outputFilePath=path.join(__dirname, '../public/output/2-matches-won-per-team-per-year.json');

exports.getMatchesWonPerTeam = (req, res, next) => {

    csvToJson()
    .fromFile(matchesFile)
    .then((result)=>{
        const matchesWonPerTeam = result.reduce((acc, value) => {
            if(value.winner && value.season) {
                const winningTeam = value.winner.trim();
                const year = value.season;
                const team1 = value.team1.trim();
                const team2 = value.team2.trim();
                if(!acc[year]) {
                    acc[year] = {};
                    acc[year][team1] = 0;
                    acc[year][team2] = 0;
                    // increamenting winning team's count
                    acc[year][winningTeam] ++;
                } else {
                    if(!acc[year][team1]) {
                        // creating team1's entry if it doesn't exist
                        acc[year][team1] = 0;
                    }
                    if(!acc[year][team2]) {
                        // creating team2's entry if it doesn't exist
                        acc[year][team2] = 0;
                    }
                    // increamenting win count of winning team
                    acc[year][winningTeam] ++;
                }
            } else {
                acc = { ...acc };
            }
            return acc;
        }, {});
        
        res.json(matchesWonPerTeam);
        // fs.writeFileSync(outputFilePath, JSON.stringify(matchesWonPerTeam, null, 2), 'utf-8');
    })
    .catch(err => {
        // console.log(err);
        next(err);
    });
}