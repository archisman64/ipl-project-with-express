//  Find the bowler with the best economy in super overs

const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');

// const matchesFilePath=path.join(__dirname ,'../data/matches.csv');
const delivieriesFilePath=path.join(__dirname, '../../data/deliveries.csv');
// const outputFilePath=path.join(__dirname, '../public/output/9-bowler-with-the-best-economy-in-super-overs.json');

exports.getSuperOverEconomy = (req, res, next) => {
    csvToJson()
    .fromFile(delivieriesFilePath)
    .then((deliveries) => {
        // console.log(deliveries)
        // iterating through deliveries
        const map= deliveries.reduce((acc, delivery) => {
            const bowler = delivery.bowler.trim();
            const isSuperOver = delivery.is_super_over;
            const runsGiven = +delivery.total_runs;
    
            if(isSuperOver !== '0' && bowler) {
                // filtering out super-over deliveries
                if(!acc[bowler]) {
                    // creating new record for non existing bowler
                    acc[bowler] = {};
                    acc[bowler]['runs_given'] = runsGiven;
                    acc[bowler]['balls'] = 1;
                } else {
                    // incrementing existing bowler's stats
                    acc[bowler]['runs_given'] += runsGiven;
                    acc[bowler]['balls'] ++;
                }
            }
            return acc;        
        }, {});
        
        const resultArr = Object.keys(map).map((bowler) => {
            const info = map[bowler];
            const economy = (info['runs_given']/info['balls']) * 6;
            return [bowler, economy];
        });
        
        // console.log(resultArr);
        
        const sortedResult = resultArr.sort((bowler1, bowler2) => {
            return bowler1[1] - bowler2[1]
        });
        const topBowler = Object.fromEntries([sortedResult[0]]);
        // console.log(topBowler)
        res.json(topBowler);
        // fs.writeFileSync(outputFilePath, JSON.stringify(topBowler, null, 2), 'utf-8');
    })
    .catch((err) => {
        next(err)
    });

}
