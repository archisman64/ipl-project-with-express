const express = require('express');
const router = express.Router();

// Import the controllers for each route
const matchesPerYearController = require('../controllers/1-matches-per-year.cjs');
const matchesWonPerTeamController = require('../controllers/2-matches-won-per-team-per-year.cjs');
const extraRunsPerTeamController = require('../controllers/3-extra-runs-conceded-per-team.cjs');
const topTenEconomyController = require('../controllers/4-top-10-economical-bowlers.cjs');
const tossAndMatchWinController = require('../controllers/5-number-of-times-each-team-won-the-toss-and-also-won-the-toss.cjs');
const playerOfTheMatchAwardsController = require('../controllers/6-player-who-has-won-the-highest-number-of-player-of-the-match-awards-for-each-season.cjs');
const batsmanStrikeRateController = require('../controllers/7-strike-rate-of-a-batsman-for-each-season.cjs');
const playerDismissalController = require('../controllers/8-highest-number-of-times-a-player-has-dismissed-another-player.cjs');
const superOverEconomyController = require('../controllers/9-bowler-with-the-best-economy-in-super-overs.cjs');

const erorHandler = require('../middleware/errorHandler');

// Define the routes and their corresponding controller methods
router.get('/matches-per-year', matchesPerYearController.getMatchesPerYear, erorHandler);
router.get('/matches-won-per-team', matchesWonPerTeamController.getMatchesWonPerTeam, erorHandler);
router.get('/extra-runs-per-team', extraRunsPerTeamController.getExtraRunsPerTeam, erorHandler);
router.get('/top-ten-economy', topTenEconomyController.getTopTenEconomy, erorHandler);
router.get('/toss-and-match-win', tossAndMatchWinController.getTossAndMatchWin, erorHandler);
router.get('/player-of-the-match-awards', playerOfTheMatchAwardsController.getPlayerOfTheMatchAwards, erorHandler);
router.get('/batsman-strike-rate', batsmanStrikeRateController.getBatsmanStrikeRate, erorHandler);
router.get('/player-dismissal', playerDismissalController.getPlayerDismissal, erorHandler);
router.get('/super-over-economy', superOverEconomyController.getSuperOverEconomy, erorHandler);
// Add other routes here

module.exports = router;
