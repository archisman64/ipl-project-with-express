const express = require('express');
const router = express.Router();

const logController = require('../controllers/logController');
const erorHandler = require('../middleware/errorHandler');

router.get('/logs', logController.getRequestLogs, erorHandler); 

module.exports = router;