const express = require('express');
const router = express.Router();

const homeController = require('../controllers/homeController');
const erorHandler = require('../middleware/errorHandler');

router.get('/', homeController.getHome, erorHandler); 

module.exports = router;