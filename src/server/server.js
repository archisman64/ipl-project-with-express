const express = require('express');
const app = require('../app');

const PORT = process.env.PORT || 3000;

console.log('server.js ran');
// Set up any additional server configurations or middleware here

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
