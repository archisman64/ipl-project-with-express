const fs = require('fs');
const path = require('path');

const logsDirectory = path.join(__dirname, '../../logs');

// Create the logs directory if it doesn't exist
if (!fs.existsSync(logsDirectory)) {
  fs.mkdirSync(logsDirectory);
}